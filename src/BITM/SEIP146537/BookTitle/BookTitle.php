<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct(){



        parent::__construct();

    }
    public function index(){
        echo "Test of BookTitle class";
        echo "<br>";
        echo $this->book_title;
    }

    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){

            $this->id=$postVariabledata['id'];
        }

        if(array_key_exists('book_title',$postVariabledata)){
            $this->book_title=$postVariabledata['book_title'];
        }

        if(array_key_exists('author_name',$postVariabledata)){
            $this->author_name=$postVariabledata['author_name'];
        }
    }

    public function store(){

        $arrData=array($this->book_title, $this->author_name);
        $sql="INSERT INTO book_title(book_title, author_name) VALUES (?,?)";
       // $sql="insert INTO book_title(book_title, author_name) VALUES ('$this->book_title', '$this->author_name')"; // for stop SQL Injection Hacking we need to use value(?,?)

        $STH=$this->DBH->prepare($sql);    // $sql returning object in $STH. prepare method returns object.

        //$STH->execute();
        $result=$STH->execute($arrData);

        if($result)
            Message::Message("Success! Data Has Been Inserted Successfully :)");
            else
                Message::Message("Failed! Data Has not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }// end of store method

}